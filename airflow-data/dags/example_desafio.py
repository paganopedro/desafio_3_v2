from airflow.utils.edgemodifier import Label
from datetime import datetime, timedelta
from textwrap import dedent
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from airflow import DAG
from airflow.models import Variable
import csv
import sqlite3
import pandas as pd

# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],# emailllllllllllllllllllllll
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}
#----------------DO YOUR THING---------------------------------
def extration_data():
    caminho = "/home/pedro/Documents/desafio_3/airflow_tooltorial/data/Northwind_small.sqlite"
    con = sqlite3.connect(caminho)

    data2 = pd.read_sql('''SELECT * FROM "Order"''', con)
    data2.to_csv('output_orders.csv', index=True)

def extration_and_transform ():
    caminho = "/home/pedro/Documents/desafio_3/airflow_tooltorial/data/Northwind_small.sqlite"
    con = sqlite3.connect(caminho)

    data = pd.read_csv("output_orders.csv")
    data2 = pd.read_sql('''SELECT * FROM "OrderDetail"''', con)
    result = pd.merge(data, data2, left_on = ["Id"], right_on=["OrderId"])

    result = result[result["ShipCity"] == "Rio de Janeiro"]
    result= str(result["Quantity"].sum())

    text_file = open("count.txt", "w")
    text_file.write(result)
    
 
 #-------------STOP DOING YOUR THING--------------------------


## Do not change the code below this line ---------------------!!#
def export_final_answer():
    import base64

    # Import count
    with open('count.txt') as f:
        count = f.readlines()[0]

    my_email = Variable.get("my_email")
    message = my_email+count
    message_bytes = message.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes)
    base64_message = base64_bytes.decode('ascii')

    with open("final_output.txt","w") as f:
        f.write(base64_message)
    return None
## Do not change the code above this line-----------------------##



with DAG(
    'DesafioAirflow',
    default_args=default_args,
    description='Desafio de Airflow da Indicium',
    schedule_interval=timedelta(days=1),
    start_date=datetime(2021, 1, 1),
    catchup=False,
    tags=['example'],
) as dag:
    dag.doc_md = """
        Esse é o desafio de Airflow da Indicium.
    """

# ------------------------MINHAS DAG------------------------------

    extract_data_db = PythonOperator(
        task_id = 'extration_info_from_db',
        python_callable = extration_data 
    )
    extract_transform_txt = PythonOperator(
        task_id = 'extract_transform_txt',
        python_callable = extration_and_transform 
    )

# ------------------------FIM DAS MINHAS DAG------------------------------

    export_final_output = PythonOperator(
        task_id='export_final_output',
        python_callable=export_final_answer,
        provide_context=True
    )


# ----------minha info de orquestação------------------------------------
extract_data_db >> extract_transform_txt >> export_final_output
# ------------------------FIM DAS INFO DE ORQUESTAÇÃO------------------------------