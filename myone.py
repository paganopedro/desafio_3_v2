import pandas as pd
import csv
import sqlite3

""" -------------------------------------------------------------------------------------------
                FUNÇÃO QUE ACESSA O BANCO DE DADOS 'ORDER' E EXPORTA UM ARQUIVO CSV DE
 ------------------------------------------------------------------------------------------- """

caminho = "/home/pedro/Documents/desafio_3/airflow_tooltorial/data/Northwind_small.sqlite"
con = sqlite3.connect(caminho)

data2 = pd.read_sql('''SELECT * FROM "Order"''', con)
data2.to_csv('output_orders.csv', index=True)
 
