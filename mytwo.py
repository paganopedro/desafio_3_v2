import pandas as pd
import sqlite3


""" -------------------------------------------------------------------------------------------
     FUNÇÃO QUE ACESSA O BANCO DE DADOS 'ORDERDETAILS' FAZ JOIN E CALCULA E EXPORTA STR()
 ------------------------------------------------------------------------------------------- """

caminho = "/home/pedro/Documents/desafio_3/airflow_tooltorial/data/Northwind_small.sqlite"
con = sqlite3.connect(caminho)

data = pd.read_csv("output_orders.csv")
data2 = pd.read_sql('''SELECT * FROM "OrderDetail"''', con)
result = pd.merge(data, data2, left_on = ["Id"], right_on=["OrderId"])

result = result[result["ShipCity"] == "Rio de Janeiro"]
result= str(result["Quantity"].sum())

text_file = open("count.txt", "w")
text_file.write(result)




